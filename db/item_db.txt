#ID,	Name,			Label,		Attr,	Type,	Price,	Sell,	Weight,	ATK,	DEF,	Range,	Mbonus,	Slot,	Gender,	Loc,	wLV,	eLV,	View,	LifeTime,		{UseScript},				{EquipScript}				{UnEquipScript}
0,	DEFAULT,			Default,		0,	0,	0,	0,	10,	,	,	,	,	,	2,	,	,	,	,	0,		{},					{}	
501,	Acorn,			Acorn,		0,	0,	0,	0,	2,	,	,	,	,	,	2,	,	,	0,	,	0,		{ itemheal 15, 0; },			{}
502,	Piberries,		Piberries,	0,	0,	0,	0,	3,	,	,	,	,	,	2,	,	,	0,	,	0,		{ itemheal 20, 0; },			{}
503,	Cheese,			Cheese,		0,	0,	0,	0,	10,	,	,	,	,	,	2,	,	,	0,	,	0,		{ itemheal 100, 0; },			{}
504,	Honey,			Honey,		0,	0,	0,	0,	5,	,	,	,	,	,	2,	,	,	0,	,	0,		{ itemheal 50, 0; },			{}
505,	Carrot,			Carrot,		0,	0,	0,	0,	10,	,	,	,	,	,	2,	,	,	0,	,	0,		{ itemheal 35, 0; },			{}
506,	Lettuce,			Lettuce,		0,	0,	0,	0,	5,	,	,	,	,	,	2,	,	,	0,	,	0,		{ itemheal 50, 0; },			{}
507,	Bread,			Bread,		0,	0,	0,	0,	10,	,	,	,	,	,	2,	,	,	0,	,	0,		{ itemheal 70, 0; },			{}
508,	Pious'legs,		Pious' legs,	0,	0,	0,	0,	10,	,	,	,	,	,	2,	,	,	0,	,	0,		{ itemheal 25, 0; },			{}
700,	Threeleafclover,		Three-leaf clover,	0,	3,	0,	0,	2,	,	,	,	,	,	2,	,	,	0,	,	0,		{},					{}
701,	Brokenknife,		Broken knife,	0,	3,	0,	0,	15,	,	,	,	,	,	2,	,	,	0,	,	0,		{},					{}
702,	Rattail,			Rat tail,		0,	3,	0,	0,	5,	,	,	,	,	,	2,	,	,	0,	,	0,		{},					{}
703,	Rattooth,			Rat tooth,	0,	3,	0,	0,	2,	,	,	,	,	,	2,	,	,	0,	,	0,		{},					{}
704,	Root,			Root,		0,	3,	0,	0,	7,	,	,	,	,	,	2,	,	,	0,	,	0,		{},					{}
705,	Abela'salvolus,		Abela's alvolus,	0,	2,	0,	0,	25,	,	,	,	,	,	2,	,	,	0,	,	0,		{ doevent "ServiceNPC::OnAlvolusUse"; }		{}
706,	Abela'sskin,		Abela's skin,	0,	3,	0,	0,	10,	,	,	,	,	,	2,	,	,	0,	,	0,		{},					{}
707,	Abelaswing,		Abela's wing,	0,	3,	0,	0,	5,	,	,	,	,	,	2,	,	,	0,	,	0,		{},					{}
708,	Tortugastongue,		Tortugas's tongue,	0,	3,	0,	0,	5,	,	,	,	,	,	2,	,	,	0,	,	0,		{},					{}
709,	Squichy'sclaw,		Squichy's claw,	0,	3,	0,	0,	5,	,	,	,	,	,	2,	,	,	0,	,	0,		{},					{}
710,	Pious'sfeathers,		Pious's feathers,	0,	3,	0,	0,	5,	,	,	,	,	,	2,	,	,	0,	,	0,		{},					{}
711,	Halfeggshell,		Half eggshell,	0,	3,	0,	0,	5,	,	,	,	,	,	2,	,	,	0,	,	0,		{},					{}
712,	Tortugasshellfragment,	Tortugas shell fragment,0	3,	0,	0,	5,	,	,	,	,	,	2,	,	,	0,	,	0,		{},					{}
1000,	Fourleafclover,		Four-leaf clover,	0,	5,	0,	0,	2,	,	,	,	,	,	2,	1024,	,	0,	,	0,		{},					{ bonus bLuk, 2; }
1001,	Beginnersamulet,		Beginner's amulet,	0,	5,	0,	0,	50,	,	,	,	,	,	2,	1024,	,	0,	,	0,		{},					{ bonus bVit, 1; }
1300,	CottonShirt,		Cotton Shirt,	0,	5,	0,	0,	50,	,	,	,	,	,	2,	512,	,	0,	,	0,		{},					{}
1301,	SailorShirt,		Sailor Shirt,	0,	5,	0,	0,	50,	,	5,	,	,	,	2,	512,	,	0,	,	0,		{},					{}
1302,	Shorttanktop,		Short tank top,	0,	5,	0,	0,	70,	,	,	,	,	,	2,	512,	,	0,	,	0,		{},					{}
1303,	Sorcererrobe,		Ssorcerer robe,	0,	5,	0,	0,	150,	,	,	,	,	,	2,	512,	,	0,	,	0,		{},					{}
1800,	Furboots,			Fur boots,	0,	5,	0,	0,	55,	,	3,	,	,	,	2,	64,	,	0,	,	0,		{},					{}
1801,	Leatherboots,		Leather boots,	0,	5,	0,	0,	50,	,	2,	,	,	,	2,	64,	,	0,	,	0,		{},					{}
1802,	Boots,			Boots,		0,	5,	0,	0,	30,	,	1,	,	,	,	2,	64,	,	0,	,	0,		{},					{}
1803,	Sandal,			Sandal,		0,	5,	0,	0,	15,	,	1,	,	,	,	2,	64,	,	0,	,	0,		{},					{}
2000,	Gloves,			Gloves,		0,	5,	0,	0,	20,	,	1,	,	,	,	2,	4,	,	0,	,	0,		{},					{}
2001,	Bracelets,		Bracelets,	0,	5,	0,	0,	30,	,	2,	,	,	,	2,	4,	,	0,	,	0,		{},					{}
2200,	Pants,			Pants,		0,	5,	0,	0,	100,	,	,	,	,	,	2,	1,	,	0,	,	0,		{},					{}
2201,	Miniskirt,		Mini-skirt,	0,	5,	0,	0,	70,	,	,	,	,	,	2,	1,	,	0,	,	0,		{},					{}
2202,	Shorts,			Shorts,		0,	5,	0,	0,	80,	,	,	,	,	,	2,	1,	,	0,	,	0,		{},					{}
2500,	Angelwings,		Angel wings,	0,	5,	0,	0,	200,	,	,	,	,	,	2,	8,	,	0,	,	0,		{},					{ bonus bAddSpeed, 15; }
2700,	Barrel,			Barrel,		0,	5,	0,	0,	600,	,	20,	,	20,	,	2,	32,	,	0,	,	0,		{},					{ bonus bAddSpeed, -30; bonus bCriticalDef, 15; bonus bAtkRange, -5; }
2701,	Woodenshield,		Wooden shield,	0,	5,	0,	0,	600,	,	,	,	,	,	2,	32,	,	0,	,	0,		{},					{}
2900,	Circlet,			Circlet,		0,	5,	0,	0,	35,	,	,	,	,	,	2,	256,	,	0,	,	0,		{},					{}
2901,	Funkyhat,			Funky hat,	0,	5,	0,	0,	30,	,	,	,	,	,	2,	256,	,	0,	,	0,		{},					{}
2902,	Witchhat,			Witch hat,	0,	5,	0,	0,	30,	,	,	,	,	,	2,	256,	,	0,	,	0,		{},					{}
3200,	Scarf,			Scarf,		0,	5,	0,	0,	20,	,	,	,	,	,	2,	128,	,	0,	,	0,		{},					{}
3500,	Deathblossom,		Death blossom,	0,	4,	0,	0,	100,	35,	,	2,	,	0,	2,	2,	1,	1,	1,	0,		{},					{}
3501,	Broadsword,		Broadsword,	0,	4,	0,	0,	100,	15,	,	1,	,	0,	2,	2,	1,	1,	1,	0,		{},					{}
#	<------ 23chars ------>	<------ 23chars ------>	
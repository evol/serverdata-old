// Evol scripts.
// Authors:
//    4144
// Description:
//    Scissor script. Randomly change player hair style.

function	script	scissorsaction	{
    set @style, rand(0, 22);
    gmcommand "@hairstyle " + @style;
    return;
}

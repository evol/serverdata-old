// Evol scripts.
// modified change sex script from tmw
// Authors:
//    4144
// Description:
//    Change player sex

002-3-0.gat,29,28,0,1	script	Eurni	300,{
    set @money, 10;
    if (BaseLevel < 10) goto L_TooYoung;
    if (zeny < @money) goto L_NoMoney;

    mesn l("Eurni the Surgeon");
    mesq l("Are you tired of being what you are?");
    next;

    mesn l("Eurni the Surgeon");
    mesq l("Would you be interested in a sex change?");
    next;

    mesn l("Server");
    mes l("Warning: All characters under this login will be changed.") + " " + l("Once it's done, you will be kicked from the server. Don't panic, as everything is fine.");
    next;

    menu
        l("Please do, my dear") + "...", L_Change,
        l("Leave alone my family treasure!"), -;
    close;

L_Change:
    if (zeny < @money) goto L_No_Money;
    set zeny, zeny - @money;
    changesex;
    close;

L_TooYoung:
    mesn l("Eurni the Surgeon");
    mesq l("Move along, kid.");
    close;

L_NoMoney:
    mesn l("Eurni the Surgeon");
    mesq l("You don't have enough to pay for my services.");
    close;
}
